%title: Beginners Guide to Python
%author: Chad Hirsch (Charims)
%date: 2021-10-16


-> Beginners Guide to Python <-
===============================

---

-> Who Am I <-
==============


-> Chad Hirsch <-

- Father and Husband
- TSgt in the NVANG
- DevOps Engieer by day
- Programmer at night
- Self hoster on my free time
- All around Geek

---

-> Python <-
============

-> A Easy To Learn Programming Language
-> JIT Byte Coded
-> Used by Organizations big and small

---

-> What this course is <-
=========================

- Learn when and why to use python
- See how easy it can be
- Maybe Solve some small problems
- Learn how you can learn more!

---

-> Want More? <-
===============

- Programming
- Scripting
- Automation
- DevOps
- IT Team development
- Living the IT life
- Incident Managment
- Workflow Management
- The *CLOUD*
- Risk Management
- IT security (An engineer's perspective)

---
-> Contact me <-
================

- Official Email: chad.hirsch@us.af.mil
- Personal Email: charims@pcwideopen.com
- Matrix: @charims:pcwideopen.com
- Fediverse: charims@pcwideopen.com
- Website: [https://chad.hirsch.host](https://chad.hirsch.host)

---

-> Popular Sofware written in python <-
=======================================

-> Youtube
-> Google
-> Instagram
-> Instagram
-> Reddit
-> Spotify
-> Dropbox

---

-> Where Python runs <-
=======================

- Linux
- Unix
- MacOS
- Android
- IOS
- Windows


---

-> Installation <-

- [https://wiki.python.org/moin/BeginnersGuide/Download](https://wiki.python.org/moin/BeginnersGuide/Download)
- Your local package manager (apt, yum, pacman, zypper)
- The Wind0w$ store



---

-> Some Tools to you might like <-

- IDEs:
  - Vim
  - PyCharm
  - Sublime Text
  - Atom
  - VScodium

- Make your code pretty:
  - pylint
  - pre-commit

- Manage your python environment:
  - pipenv
  - virtualenv

- Presentation tooling:
  - mdp
  - markdown

---
-> Lets get started <-




- Things are about to get scary
- Don't run away just yet
- *Don't Panic!* 


--- 
-> Basic Syntax <-


```
varname = "value"
# this is a comment
is_this_on = false

mylist = ["a", "b", "c"]
for letter in mylist:
    print(letter)

#Lets make a function
def myFunction(var=None):
    """This is a
    Doc String """
    frobinate_my_stack(var)
    # !indentation controls execution!

```


---

-> Practical <-
==============


---

-> Questions? <-

