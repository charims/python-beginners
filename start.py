"""
A python file showing how python runs
"""



# We can put our code here
name = "paul olson"
mynumber = 1
mylist = [ 1,2,3,4,5,6,7 ]

#print(name)
#print(mynumber)
#print(mylist)

# Python has minimal syntax, which makes it easy to write

#print(name.upper())
#print(name)
#name = name.upper()
#print(name)

# Python is meant to be very readable



#Lets do a loop
mylist = ["a", "b", "c"]
mylist2 = []
for letter in mylist:
    mylist2.append(letter.upper())
    print(letter)

for letter in mylist2:
    print(letter)


name = "Charles Schwartz"
#print(name.split(" "))

#namelist = name.split(" ")
#firstname = namelist[0]
#print(firstname)
#
#
#lastname = namelist[1]
#print(lastname)
#



#Lets make a function
def separatenames(myname):
   """
   Outputs a first and last name as separate variables
   """
   mynamelist = myname.split(" ")
   fname = mynamelist[0]
   lname = mynamelist[1]
   return fname, lname 


firstname, lastname = separatenames(name)
print(firstname)
print(lastname)

firstname1, lastname1 = separatenames("Jose Garcia")
print(firstname1)
print(lastname1)

#Lets call our function


# Scoping

def myFunction():
    pass

print("")
