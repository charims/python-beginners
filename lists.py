"""
This is a beginner's to python file
"""


mydict = {
    "cats": 2,
    "dogs": 3
    }
print(mydict)

print(mydict["cats"] + mydict["dogs"])

#roster = [
#    {
#        "firstname": "Paul",
#        "lastname" : "dugal"
#        },
#    {   "firstname": "susan",
#        "lastname" : "rice"
#        }
#    ]
#
#for student in roster:
#    print(student['firstname'])
#

def create_student(student_name, age, student_id):
    """
    Creates a student for the roster from their name and age, returns student as a dictionary
    """
    student_title = student_name.title()
    student_fname = student_title.split(" ")[0]
    student_lname = student_title.split(" ")[1]
    student = {
        "firstname": student_fname,
        "lastname": student_lname,
        "age": age,
        "id": student_id
        }
    return student

def main():
    """
    This is the main function
    """
    students = [
        "Paul dugal",
        "susan rice",
        "freDDie muRPHIE",
        "Annie OAKLeY",
        "geoRgE Castanza",
        "Niel Flaherty"
        ]
    ages = [
            32,
            18,
            48,
            30,
            23,
            50
            ]
    roster = []
    for student_id in range(0, len(students)):
        roster.append(
            create_student(
                students[student_id],
                ages[student_id],
                student_id
                )
            )
    for student in roster:
        print(student)

if __name__ == "__main__":
    main()
