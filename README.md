# Python For Beginners

Welcome to my python for beginners course. This is just meant to show you
what's possible for python, and how easy it can be to accomplish a task or
solve a problem using python. You don't have to be a programmer to use python,
it can help you solve everday repetative problems in your computer workflow.


## Slides
You can install a tool called [mdp](https://github.com/visit1985/mdp) if you want to view the slides as they
were presented. Just run `mdp slides.md`. Otherwise, just view them with
your favorite text editor or markdown viewer, or just view it through [gitlab](https://gitlab.com/charims/python-beginners/-/blob/master/slides.md)

## Links
Just take a look at [the links here](https://gitlab.com/charims/python-beginners/-/blob/master/links.txt).

## How to install python:

https://www.python.org/downloads/
https://www.howtogeek.com/197947/how-to-install-python-on-windows/



